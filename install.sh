#!/bin/bash

# To install MDS, run this command from your project folder
# curl -fsSL https://gitlab.com/miastudio/mds/-/raw/master/install.sh | bash /dev/stdin install
# For more information, refer to Readme on https://gitlab.com/miastudio/mds

function install() {
  echo "Installing MDS"

  submodule=$(grep path .gitmodules | sed 's/.*= //' | grep mds)
  if [[ "$submodule" == *"mds"* ]]; then
    echo "MDS is already installed"
    return
  fi
  echo "Adding MDS as a subproject"
  # Disabled as SSH is simpler for 
  # git submodule add https://gitlab.com/miastudio/mds.git
  git submodule add git@gitlab.com:miastudio/mds.git

  echo "Configuring MDS defaults"
  file=".env-default"
  if [ -f "$file" ]; then
    echo "File $file already exists. Skipping to avoid overwriting."
  else
    cp mds/$file ./$file
  fi
  file=".env"
  if [ -f "$file" ]; then
    echo "File $file already exists. Skipping to avoid overwriting."
  else
    cp mds/$file.sample ./$file
  fi
  file="makefile"
  if [ -f "$file" ]; then
    echo "File $file already exists. Skipping to avoid overwriting."
  else
    ln -s mds/$file ./$file
  fi
  file=".dockerignore"
  if [ -f "$file" ]; then
    echo "File $file already exists. Skipping to avoid overwriting."
  else
    ln -s mds/$file.sample ./$file
  fi

  echo "Note: Consider using mds/mds.php as your sample database credentials retrieval system"

  echo "Installation completed"
  remove_script
}

function uninstall() {
  echo "Uninstalling MDS"
  git rm -f mds
  rm -rf .git/modules/mds
  remove_script
}

function update() {
  git submodule update --init --force --remote
}

function remove_script() {
  while true; do
    read -p "Do you wish to remove this installation script '$0' [y/n]?" yn
    case $yn in
    [Yy]*)
      rm $0
      break
      ;;
    [Nn]*) return ;;
    *) return ;;
    esac
  done
}

echo "MDS installer. Make sure to run this script inside your project folder (move out of mds folder if you have MDS installed)."
case "$1" in
"install")
  install
  ;;
"uninstall")
  uninstall
  ;;
"update")
  update
  ;;
*)
  echo "Usage: $0 [command]"
  echo "Commands:"
  echo " - install"
  echo " - uninstall"
  echo " - update"
  exit 1
  ;;
esac
