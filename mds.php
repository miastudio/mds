<?php
if (!function_exists('getMDSEnv')) {
    function getMDSEnv($environment_var, $default)
    {
        if (getenv($environment_var)) {
            return getenv($environment_var);
        }
        return $default;
    }
}

$mds_db_host = getMDSEnv('MIA_DB_HOST', '127.0.0.1');
$mds_db_port = getMDSEnv('MIA_DB_PORT', '3306');
$mds_db_name = getMDSEnv('MIA_DB_NAME', 'db');
$mds_db_user = getMDSEnv('MIA_DB_USER', 'user');
$mds_db_pass = getMDSEnv('MIA_DB_PASS', '**SET_PASSWORD_IN_.env_FILE**');

//print($mds_db_host);
//print($mds_db_port);
//print($mds_db_name);
//print($mds_db_user);
//print($mds_db_pass);
//die();
