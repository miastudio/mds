# MiA Dockerise Script 2

## Introduction

MiA Dockerise Script (MDS) is a set collection of makefile targets to simplify the dockerisation of existing
applications - the
creation, configuration, running and management of docker containers. The script helps with development, deployment
and distribution of these apps as docker images.

MDS was built to support php-based web-apps, but should function for many other environments.

## Installation, Update and Uninstallation

### Requirements

- [docker](https://docs.docker.com/engine/install/)
- on Apple Silicon computers,
  enable [docker buildx](https://cloudolife.com/2022/03/05/Infrastructure-as-Code-IaC/Container/Docker/Docker-buildx-support-multiple-architectures-images/)
- [make](https://www.gnu.org/software/make/manual/make.html)

### Automated

Make sure you run the below commands *from your project folder*:

`cd path/to/project`

- Install: ```curl -fsSL https://gitlab.com/miastudio/mds/-/raw/master/install.sh | bash /dev/stdin install```
- Update: ```curl -fsSL https://gitlab.com/miastudio/mds/-/raw/master/install.sh | bash /dev/stdin update```
- Uninstall: ```curl -fsSL https://gitlab.com/miastudio/mds/-/raw/master/install.sh | bash /dev/stdin uninstall```

### Manual

#### Installation

```shell
cd path/to/project
git submodule add git@gitlab.com:miastudio/mds.git
touch .env-default
touch .env
ln -s mds/makefile ./makefile
ln -s mds/.dockerignore.sample ./.dockerignore
```

Note: Instead of `git@gitlab.com:miastudio/mds.git` (used in the installation script), you can also use `https://gitlab.com/miastudio/mds.git`. The advantage of using `git@` is that it will automatically work with private key authentication if you wish to contribute to MDS.

#### Update

```shell
cd path/to/project
git submodule update --init --force --remote
```

#### Uninstallation

```shell
cd path/to/project
git rm -f mds
rm -rf .git/modules/mds
```

## Usage

### Configuration

The configuration is centred around 2 files:

- `Dockerfile`: the docker image building instruction
- `.env-default`: the docker image build variables

Both of these files should be included in your project repository.

Additionally, `.env` can be used to allow individual users (such as developers) to customise their local
environment. `.env` *must **not*** be committed to your repository.

#### Dockerfile

Setting up the Dockerfile is beyond the scope of this documentation. There is plenty of documentation online. A good
place to start is https://docs.docker.com/engine/reference/builder/

This is an example `Dockerfile` used for a php7.0-based application:

```dockerfile
# Latest compatible version
FROM php:7.0.33-apache-jessie

ARG A_MIA_CONTAINER_NAME
ENV MIA_CONTAINER_NAME=$A_MIA_CONTAINER_NAME
ARG A_MIA_CONTAINER_DOCUMENTROOT
ENV APACHE_DOCUMENT_ROOT=$A_MIA_CONTAINER_DOCUMENTROOT
ARG A_MIA_CONTAINER_TIMEZONE
ENV MIA_CONTAINER_TIMEZONE=$A_MIA_CONTAINER_TIMEZONE

ARG A_MIA_APP_NAME
ENV MIA_APP_NAME=$A_MIA_APP_NAME
ARG A_MIA_APP_VERSION
ENV MIA_APP_VERSION=$A_MIA_APP_VERSION

ARG A_APP_MIA_SHOWERRORS
ENV MIA_APP_SHOWERRORS=$A_MIA_APP_SHOWERRORS
ARG A_MIA_APP_DEBUGLEVEL
ENV MIA_APP_DEBUGLEVEL=$A_MIA_APP_DEBUGLEVEL

ARG A_MIA_APP_ENVIRONMENT
ENV MIA_APP_ENVIRONMENT=$A_MIA_APP_ENVIRONMENT

# Install mcrypt
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv mcrypt \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

# Use ./www as root (using non /var/www/html structure)
COPY . /app/

# from https://hub.docker.com/_/php
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf && \
    sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN chown -R www-data:www-data /app

# Remove 'ServerName not set' error
RUN echo "ServerName $MIA_APP_NAME" >> /etc/apache2/apache2.conf

# Enable RewriteModule
# using 'apachectl restart' insted of 'service apache2 restart' to avoid errors due to error 'AH00111: Config variable ${APACHE_DOCUMENT_ROOT} is not defined'
RUN a2enmod rewrite && \
    apachectl restart
```

Note: to pass variables to the Dockerfile, the following method is used:

1. In .deploytools.sh functions, arguments are passed in a placeholder
   variable `A_XXX`: `--build-arg A_MIA_CONTAINER_NAME=${MIA_CONTAINER_NAME}`
1. In Dockerfile, the placeholder variable is defined: `ARG A_MIA_CONTAINER_NAME`
1. In Dockerfile, the environment variable (original name) is set from the
   placeholder: `ENV MIA_CONTAINER_NAME=$A_MIA_CONTAINER_NAME`

#### .env-default

This file sets all the default environment variables used during the docker image creation as well as those used during
local container execution. A complete list of all supported variables is available at the end of this file.

See [mds/.env-default.sample](https://gitlab.com/miastudio/mds/-/raw/master/.env-default.sample) for the latest example
file.

```ini
# copy this file to .env to override settings
# .env-default will be used for builds
# .env will be used for run_local

# Enable additional messages
MDS_DEBUG = true

# Image settings
MIA_IMAGE_NAME = registry.myhost.com/project/name
MIA_IMAGE_TAG = 1.2.3-beta

# Container Settings
MIA_CONTAINER_DAEMONIZE = true
#MIA_CONTAINER_VOLUME=local
MIA_CONTAINER_VOLUME = container
MIA_CONTAINER_NAME = my_app
MIA_CONTAINER_PORT = 8080
MIA_CONTAINER_DOCUMENTROOT = /app/www
MIA_CONTAINER_MEMORY = 512M
MIA_CONTAINER_TIMEZONE = Asia/Kolkata

# App settings
MIA_APP_NAME = app
MIA_APP_VERSION = 1.2.3
MIA_APP_SHOWERRORS = false
MIA_APP_DEBUGLEVEL = 5
MIA_APP_ENVIRONMENT = development

# DB Settings
MIA_DB_HOST = host.docker.internal
MIA_DB_PORT = 3306
MIA_DB_NAME = dbname
MIA_DB_USER = dbuser
MIA_DB_PASS = **SET_PASSWORD_IN_.env_FILE**

# List of servers
MIA_DEPLOY_SERVERS=staging production
# Default server to use
MIA_DEPLOY_DEFAULT_SERVER=staging
# Production server
MIA_DEPLOY_production_HOST = docker-admin-user@production.server
MIA_DEPLOY_production_KEYFILE = ~/.ssh/production_private_key
MIA_DEPLOY_production_COMMAND = cd /path/to/folder && docker compose pull && docker compose down && docker compose up -d
# Staging server
MIA_DEPLOY_staging_HOST = docker-admin-user@staging.server
MIA_DEPLOY_staging_KEYFILE = ~/.ssh/staging_private_key
MIA_DEPLOY_staging_COMMAND = cd /path/to/folder && docker compose pull && docker compose down && docker compose up -d

# Create tags for non-master releases
MIA_REPOSITORY_APPLYTAG = true
# Push container image to 'dev' when pushing to 'latest'
MIA_REPOSITORY_PUSHLATESTTODEV=true

MIA_RUN_ADDITIONALPARAMETERS = -e FLIC_SERVERPATH=$(FLIC_SERVERPATH) -e FLIC_BGCOLOR=$(FLIC_BGCOLOR) -e FLIC_HEADER=$(FLIC_HEADER)
#MIA_RUN_OVERRIDE=docker run....
```

#### .env

This file sets all the local (custom) settings of the environment. It allows for overrides during image builds and/or
container execution.

The syntax and content is the same as for `.env-default`.

#### .dockerignore

When images are built using Dockerfile and the `ADD` or `COPY` command, the filenames contained in `.dockerignore`
will be omitted from the copy to the container image.  
This can be useful to remove unnecessary or unwanted files from being deployed in public images or reducing container
size. For further information,
see [Docker documentation](https://docs.docker.com/engine/reference/builder/#dockerignore-file).

If the file does not exist, it will be created as a link to the MDS file (ensuring updates are added in the future)
If you wish to customise the file, delete the link, copy `mds/.dockerignore` into your development folder and edit as
required.

The provided template ignores the following files by default:

- `.DS_Store`
- `.dockerignore`
- `.env`
- `.env-default`
- `.git`
- `.gitignore`
- `.gitmodules`
- `.idea`
- `Dockerfile`
- `docker-compose.yml`
- `/mds`
- `deploy.sh`

### Examples

#### Get help

`make` or `make help`

```shell
❯ make help
MiA Deployment Script (MDS) v2.0.4
backup	: Backup database file=<path/to/file.sql.gz>, or MIA_DB_NAME.sql.gz by default
build	: Build container image
delete	: Delete container image
deploy	: Deploy to server server=<server>, or MIA_DEPLOY_DEFAULT_SERVER by default
help	: Show this help
login	: Login to server server=<server>, or MIA_DEPLOY_DEFAULT_SERVER by default
publish	: Same as release
rebuild	: Rebuild container
release	: Release container image to registry
remove	: Remove container (if it exists)
restart	: stop + start
restore	: Restore database file=<path/to/file.sql.gz>, or MIA_DB_NAME.sql.gz by default
run	: Run container (create it if it does not exist)
start	: Same as run
stop	: Stop container (if it exists and is running)
```

#### Run a development container

Recommended settings in `.env`:

```ini
# Container Settings
MIA_CONTAINER_DAEMONIZE = false
MIA_CONTAINER_VOLUME = local

# Optional
MDS_DEBUG = true
```

- Build image: `make build` (or `make rebuild` if previously built)
- Run image: `make run`

This will run the container with the app code mounted from the local filesystem into the container. Make changes to
code, refresh in browser to see changes. Press ctrl-c to stop the container.

#### Run a production container

Recommended settings in `.env`:

```ini
MIA_CONTAINER_DAEMONIZE = true
# Should already be default in .env-default - if so, not required
MIA_CONTAINER_VOLUME = container
MDS_DEBUG = false
```

- Build image: `make build` (or `make rebuild` if previously built)
- Run image: `make run`

This will run the container with the app from the built-in filesystem and will restart the container if the machine
restarts.

Example:

##### Build

Builds the container.
- When run directly, builds a container image for the current CPU architecture
- When run via publish, and running on Apple Silicon, builds a multi-platform container image for `linux/arm64` and `linux/amd64`

```shell
❯ make build
Building container for registry.myhost.com/project/name
    docker \
		build \
		\
		\
		\
		--build-arg A_MIA_CONTAINER_NAME=my_app \
		--build-arg A_MIA_CONTAINER_DOCUMENTROOT=/app/www \
		--build-arg A_MIA_CONTAINER_TIMEZONE=Asia/Kolkata \
		\
		--build-arg A_MIA_APP_SHOWERRORS=false \
		--build-arg A_MIA_APP_DEBUGLEVEL=0 \
		--build-arg A_MIA_APP_NAME=App \
		--build-arg A_MIA_APP_VERSION=1.2.3 \
		--build-arg A_MIA_CONTAINER_MEMORY= \
		\
		--build-arg A_MIA_DB_HOST= \
		--build-arg A_MIA_DB_PORT= \
		--build-arg A_MIA_DB_NAME= \
		--build-arg A_MIA_DB_USER= \
		--build-arg A_MIA_DB_PASS= \
		\
		\
		\
		-t registry.myhost.com/project/name \
		\
		\
		\
```
Tags in local builds:
- local builds will have the tag `dev`
- if `MIA_REPOSITORY_PUSHLATESTTODEV` is set to `true`, the following tag will also be added: `latest` 

See publish section for more information on tags

##### Run

Runs the container with the predetermined parameters

With `MIA_CONTAINER_DAEMONIZE=true`

```shell
❯ make run
Starting my_app container.
 • Running on Apple silicon.
 • Using HTTP port 8013
 • Opening browser for you at http://127.0.0.1:8013

Running in daemon mode. Run make stop to stop.

Creating the container.
04361bd28d4b6a0c5e6595306a3625af809db0a2b13e0cf467e649ac6919d99d
[Done]
```

With `MIA_CONTAINER_DAEMONIZE=false`

```shell
❯ make run
Starting my_app container.
 • Running on Apple silicon.
 • Using HTTP port 8013
 • Opening browser for you at http://127.0.0.1:8013

Running in interactive mode. Press Ctrl+C to stop.

Creating the container.
[Mon Month 01 18:21:15.013157 2020] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.10 (Debian) PHP/7.0.33 configured -- resuming normal operations
[Mon Month 01 18:21:15.015838 2020] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
```

##### Publish

This uploads the image to a repository using the following rules for tags:

| current git branch     | `master` or `main` ("production") | other ("beta")                 |
|------------------------|-----------------------------------|--------------------------------|
| image tag: `[version]` | `$MIA_IMAGE_TAG`                  | `$MIA_IMAGE_TAG-timestamp`     |
| image tag: `dev`             | no                                | yes                            |
| image tag: `prod`            | yes                               | no                             |
| image tag: `latest`          | yes                               | MIA_REPOSITORY_PUSHLATESTTODEV |
| git: apply tag         | no                                | yes                            |

Detailed explanation:
- If the git branch is set to `master` or `main`, the release is considered _production_; otherwise release isis considered _beta_
- `Production` releases are tagged with `$MIA_IMAGE_TAG` while `beta` releases are tagged with `$MIA_IMAGE_TAG` (`[version]`) appended by a timestamp
- production releases are also tagged as `prod`
- beta releases are also tagged as `dev`
- If `MIA_REPOSITORY_PUSHLATESTTODEV` is set to true, beta releases will also be tagged `latest`
- If `MIA_REPOSITORY_APPLYTAG` is set to true, a similar tag (`b$MIA_IMAGE_TAG-timestamp`) is applied to the git repository for beta releases
- When run on Apple Silicon, it will launch a builds for a multi-platform container image for `linux/arm64` and `linux/amd64`. Otherwise, will build for current platform only.
 

```shell
❯ make publish
You are attempting to release git branch development
This will be published to branch dev instead of 'latest'
Your version will be appended with the timestamp: 1.2.3.20211221
This will rebuild and release to registry.myhost.com/project/name:1.2.3.20211221. Do you confirm [y/N]? [YES]
Another release of registry.myhost.com/project/name:1.2.3.20211221 already exists. Do you want to replace it [y/N]? [YES]
Building container for registry.myhost.com/project/name
docker \
		buildx build \
		--push \
		--platform linux/arm64,linux/amd64 \
		\
		--build-arg A_MIA_CONTAINER_NAME=my_app \
		--build-arg A_MIA_CONTAINER_DOCUMENTROOT=/app/www \
		--build-arg A_MIA_CONTAINER_TIMEZONE=Asia/Kolkata \
		\
		--build-arg A_MIA_APP_SHOWERRORS=false \
		--build-arg A_MIA_APP_DEBUGLEVEL=0 \
		--build-arg A_MIA_APP_NAME=flic \
		--build-arg A_MIA_APP_VERSION=1.2.3 \
		--build-arg A_MIA_CONTAINER_MEMORY= \
		\
		--build-arg A_MIA_DB_HOST= \
		--build-arg A_MIA_DB_PORT= \
		--build-arg A_MIA_DB_NAME= \
		--build-arg A_MIA_DB_USER= \
		--build-arg A_MIA_DB_PASS= \
		\
		 \
		 \
		 \
		 \
		-t registry.myhost.com/project/name:latest \
		-t registry.myhost.com/project/name:1.2.3.20211221 \
		.
[+] Building 9.9s (27/27) FINISHED
(...)
 => exporting to image
 => => exporting layers
 => => exporting manifest sha256:ae83f05e1454abb997d858ba3db6211ad9c3efa26aa2689572a94caf6899559f
 => => exporting config sha256:e6b7be9e31774c896daf55171aaef194302a2c9ef8e0efea35227863446269e0
 => => exporting attestation manifest sha256:3c36dbddb058859be61cce5f429d3e058db46595feeaa4894d6b8901631ab8aa
 => => exporting manifest sha256:3ca6c91500e72686c6c94d034b7190e9a6301227cc32521b6e0a5735b61f37fe
 => => exporting config sha256:2635264bde258f7c23a435da58371f1878c2e6795d935299a888d7f39bf52aa5
 => => exporting attestation manifest sha256:e6b595ce4630d3b9118a096fb841655b595f5a37dfe3b304dcf7a29e45979f5f
 => => exporting manifest list sha256:1571f2ae9dc3979a00d16e7003b014b9ffccd26d7f51d5cc2133df0f60590697
 => => pushing layers
 => => pushing manifest for registry.myhost.com/project/name:1.2.3.20211221@sha256:1571f2ae9dc3979a00d16e7003b014b9ffccd26d7f51d5cc2133df0f60590697
 => [auth] group/project:1.2.3.20211221:pull,push token for registry.gitlab.com
```

##### Deploy

Deploys the latest image on the repository on the server.

```shell
❯ make deploy
This will deploying to staging. Do you confirm [y/N]? [YES]
Deploying to staging
ssh -i ~/.ssh/staging_private_key docker-admin-user@staging.server "cd /path/to/program/ && docker compose pull my_app_staging && docker compose stop my_app_staging && docker compose up -d my_app_staging && docker exec my_app_staging /path/to/migration"
 my_app_staging Pulling
 my_app_staging Pulled
 Container my_app_staging  Stopping
 Container my_app_staging  Stopped
 Container my_app_staging  Created
 Container my_app_staging  Starting
 Container my_app_staging  Started
Migration run

No new migrations found. Your system is up-to-date.
```

##### Backup and Restore database

###### Backup

```shell
❯ make dbbackup
Backing up Database dbname to dbname.sql.gz
Done
```

```shell
❯ make dbbackup file=/path/to/file.sql.gz
Backing up Database dbname to /path/to/file.sql.gz
Done
```
###### Restore

```shell
❯ make restore
Restoring file dbname.sql.gz to database dbname
Done
````

```shell
❯ make restore file=/path/to/file.sql.gz
Restoring file /path/to/file.sql.gz to database dbname
Done
````

### Command-line options

#### makefile

makefile is the main code provided by MDS. The original file is in the mds folder with a symlink to the base directory.

Supported commands:

```shell
❯ make help
```

## Supported .env variables

### Image configuration

- `MIA_IMAGE_NAME` - name given to the built image; can be in the form of `username/repository` (for hub.docker.com
  hosted files) or `registry.myhost.com/project/name` for custom hosted files
- `MIA_IMAGE_TAG` - tag (`[version]`) of container to apply to container image (and push to repository).

### Container runtime configuration

- `MIA_CONTAINER_NAME` - name given to the container instance; used for creation/start/stop/deletion
- `MIA_CONTAINER_DAEMONIZE` - `true` or `false`; whether to run container in background
- `MIA_CONTAINER_VOLUME` - `local` or `container`; container will use the files as built; local will mount the working
  directory into the container allowing for development override
- `MIA_CONTAINER_VOLUME_RW` - `true` or `false`; if `true` and `MIA_CONTAINER_VOLUME=local`, will mount filesystem
  read-write (```-v `pwd`:/app:rw```)
- `MIA_CONTAINER_DOCUMENTROOT` - path in filesystem (say `/app/www`); used for server configuration
- `MIA_CONTAINER_PORT` - port for the internal webserver
- `MIA_CONTAINER_PROTOCOL` - (optional) protocol used to open container; default: `http`
- `MIA_CONTAINER_HOST` - (optional) IP/DNS used to open container; default: `127.0.0.1`
- `MIA_CONTAINER_MEMORY` - (optional) memory setting to be used in the container (for example for php's `memory_limit`)
- `MIA_CONTAINER_TIMEZONE` - (optional) timezone to use in dockerfile/container/app

### Application configuration

- `MIA_APP_NAME` - App name; used in server name and errors
- `MIA_APP_VERSION` - App version; used for tracking, usually to display to the app user. Can, but must not be the same as `MIA_IMAGE_TAG`
- `MIA_APP_SHOWERRORS` - `true` or `false`; whether to display web errors
- `MIA_APP_DEBUGLEVEL` - Debugging level; string or integer depending on app configuration
- `MIA_APP_ENVIRONMENT` - String; example `development` and `production`; can be used to set app presets

### Database configuration

- `MIA_DB_HOST` - hostname or IP; `host.docker.internal` (mac)
  [Documentation](https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach)
  and (pc) [Documentation](
  https://stackoverflow.com/questions/48546124/what-is-linux-equivalent-of-host-docker-internal/61001152)
- `MIA_DB_PORT` - port; typically `3306` for mysql
- `MIA_DB_NAME` - database name
- `MIA_DB_USER` - database username
- `MIA_DB_PASS` - database password

### Database backup and restore

Database backup and restore will use the database configuration, unless overridden by these values:
- `MIA_DBBACKUP_HOST` - hostname or IP; overrides `MIA_DB_HOST`
- `MIA_DBBACKUP_PORT` - port; overrides `MIA_DB_PORT`
- `MIA_DBBACKUP_NAME` - database name; overrides `MIA_DB_NAME`. Should probably never be used :thinking:
- `MIA_DBBACKUP_USER` - database username; overrides `MIA_DB_USER`. Set `MIA_DBBACKUP_USER=`(blank) to use current shell user
- `MIA_DBBACKUP_PASS` - database password; overrides `MIA_DBBACKUP_PASS`. Set `MIA_DBBACKUP_PASS=`(blank) to leave blank (say when root)

Database backup and restore can also be set to connect to a socket. If that is set, HOST and PORT settings will be ignored
- `MIA_DBBACKUP_USESOCKET` - `true` or `false`; whether to connect using socket (default: `false`)


### Deployment

#### General
- `MIA_DEPLOY_SERVERS` - string servernames. Must contain all the individual servernames defined below, for example `staging production`
- `MIA_DEPLOY_DEFAULT_SERVER` - (optional) string servername. Must be part of `MIA_DEPLOY_SERVERS`, for example `staging`

#### Individual servers
- `MIA_DEPLOY_<servername>_HOST` - host to do deploy to: `user@hostname`
- `MIA_DEPLOY_<servername>_KEYFILE` - path to ssh keyfile (password authentication is not supported); example `~/.ssh/id_rsa`
- `MIA_DEPLOY_<servername>_COMMAND` - command to execute remotely to perform the update. It could be something like:

```shell
cd /path/to/container/folder/ && docker compose pull my_app_staging && docker compose stop my_app_staging && docker
  compose up -d my_app_staging && docker exec my_app_staging /migration/script
```

`<servername>` can be any string. Typically `staging`, `production`, `development`, `beta`... Servername `production` is special and results in additional confirmation checks.

#### Notes on deployment
 
These can be any number of sets of identical parameters to perform automated deployments to servers.

This requires:

1. The app to have been set up on those servers
2. The servers to support ssh with public key authentication
3. The update command should be runnable as a one-liner
4. The app should be set up with a `compose.yaml` file and have a container name matching the one in the command. For
   example:

```yaml
services:
  my_app_staging:
    restart: always
    image: containers.repository.com/project/image:tag
    container_name: my_app_staging
    env_file:
      - .env-staging
```

Repeat the `my_app_staging` section as many times are required


### Misc

- `MDS_DEBUG` - `true` or `false`; whether to display MDS error messages
- `MDS_APP_NOOPEN` - `true` or `false`; default `false`. When set to true and run on macOS (Darwin) automatic browser opening on run is disabled
- `MIA_REPOSITORY_APPLYTAG` - `true` or `false`; whether the image tag (`MIA_IMAGE_TAG` + timestamp) should be tagged to the git repository during publishing
- `MIA_REPOSITORY_PUSHLATESTTODEV` - `true` or `false`; whether the image tagged as `latest` should also be tagged as `dev`
- `MIA_RUN_ADDITIONALPARAMETERS` - additional parameters provided to the `docker run...` command
- `MIA_RUN_OVERRIDE` - custom `docker run...` or other override command
- `MIA_STOP_OVERRIDE` - custom `docker stop...` or other override command
- `MIA_BUILD_ADDITIONALPARAMETERS` - additional parameters provided to the `docker build...` command

## Notes

### About docker image tags
- Local builds (`make build`) will have a `latest` tag
- Published builds (`make publish`) will have
   - If made from `master` or `main` branch: `latest`, `prod` tag and `MIA_IMAGE_TAG` (`[version]`) number tag
   - If made from another branch: `dev` and `MIA_IMAGE_TAG+timestamp` (`[version]-[timestamp]`) number tag