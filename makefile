include .env-default
# #56 and #61 exclude .env variables in these cases: build publish release rebuild release
#https://stackoverflow.com/questions/4225497/include-files-depended-on-target
#https://stackoverflow.com/questions/7656425/makefile-ifeq-logical-or
#ifneq (, $(filter $(MAKECMDGOALS), run start deploy login))
#include .env
#endif
ifeq (, $(filter $(MAKECMDGOALS), build publish release rebuild release))
include .env
endif

MDS_VERSION=2.0.9

# #22
SHELL = /bin/bash

OS=$(shell uname)
ARCH=$(shell uname -m)

ifeq ($(shell docker ps -a | grep  $(MIA_CONTAINER_NAME) | wc -l | xargs),0)
#CONTAINER_EXISTS=$(docker ps -a | grep  $(MIA_CONTAINER_NAME) && true || false)
	CONTAINER_EXISTS=false
else
	CONTAINER_EXISTS=true
endif

ifeq ($(CONTAINER_EXISTS), true)
	CONTAINER_RUNNING=$(shell docker container inspect -f '{{.State.Running}}' $(MIA_CONTAINER_NAME))
else
	CONTAINER_RUNNING=false
endif

# By default, do not push build to repository
TARGET_PUSH=
# By default, build only for current platform
TARGET_MULTIPLATFORM=
# By default, build images with `dev` tag
TAG_DEV=true
ifeq ($(MIA_REPOSITORY_PUSHLATESTTODEV),true)
	TAG_LATEST=true
else
    TAG_LATEST=
endif

# https://stackoverflow.com/questions/38801796
# Does not work if parameter is provided blank as part of command, eg 'make deploy SERVER='
# By default, containers run on 127.0.0.1
MIA_CONTAINER_HOST ?= 127.0.0.1
# By default, containers are reachable via http
MIA_CONTAINER_PROTOCOL ?= http
# Set default deployment server
ifdef MIA_DEPLOY_DEFAULT_SERVER
server ?= $(MIA_DEPLOY_DEFAULT_SERVER)
endif

ifeq ($(MIA_CONTAINER_DAEMONIZE), true)
	DAEMONIZE=-d --restart unless-stopped
else
	DAEMONIZE=--rm
endif

ifeq ($(MIA_CONTAINER_VOLUME), local)
ifeq ($(MIA_CONTAINER_VOLUME_RW), true)
	VOLUME_RW=:rw
endif
    VOLUME=-v $${PWD}:/app$(VOLUME_RW)
endif

ifeq ($(MDS_DEBUG), true)
# Nicer without ?!
#	PROGRESS="--progress=plain"
else
.SILENT:
endif

BRANCH=$(shell git rev-parse --abbrev-ref HEAD)

_SUCCESS := "\033[32m[%s]\033[0m %s\n" # Green text for "printf"
_DANGER := "\033[31m[%s]\033[0m %s\n" # Red text for "printf"
_WARN := "\033[33m[%s]\033[0m %s\n"  # Yellow text for "printf"
# https://ioflood.com/blog/bash-color/
_RED := "\\033[31m"
_BLUE := "\\033[34m"
_NORMAL := "\\033[0m"
# https://stackoverflow.com/questions/2924697/how-does-one-output-bold-text-in-bash
#_BOLD := "tput bold"
#_REGULAR := "tput sgr0"

comma := ,

MIA_RUN_ENVIRONMENTVARIABLES=-e MIA_APP_SHOWERRORS=$(MIA_APP_SHOWERRORS) \
 							 -e MIA_APP_DEBUGLEVEL=$(MIA_APP_DEBUGLEVEL) \
 							 -e MIA_APP_ENVIRONMENT=$(MIA_APP_ENVIRONMENT)


default: help


build: # Build container image
	@printf "Building container for $(_BLUE)$(MIA_IMAGE_NAME)$(_NORMAL)\n"
	docker \
		$(if $(TARGET_MULTIPLATFORM),buildx build,build) \
		$(if $(and $(TARGET_MULTIPLATFORM), $(TARGET_PUSH)),--push) \
		$(if $(TARGET_MULTIPLATFORM),--platform linux/arm64$(comma)linux/amd64) \
		\
		--build-arg A_MIA_CONTAINER_NAME=$(MIA_CONTAINER_NAME) \
		--build-arg A_MIA_CONTAINER_DOCUMENTROOT=$(MIA_CONTAINER_DOCUMENTROOT) \
		--build-arg A_MIA_CONTAINER_TIMEZONE=$(MIA_CONTAINER_TIMEZONE) \
		\
		--build-arg A_MIA_APP_SHOWERRORS=$(MIA_APP_SHOWERRORS) \
		--build-arg A_MIA_APP_ENVIRONMENT=$(A_MIA_APP_ENVIRONMENT) \
		--build-arg A_MIA_APP_DEBUGLEVEL=$(MIA_APP_DEBUGLEVEL) \
		--build-arg A_MIA_APP_NAME=$(MIA_APP_NAME) \
		--build-arg A_MIA_APP_VERSION=$(MIA_APP_VERSION) \
		--build-arg A_MIA_CONTAINER_MEMORY=$(MIA_CONTAINER_MEMORY) \
		\
		--build-arg A_MIA_DB_HOST=$(MIA_DB_HOST) \
		--build-arg A_MIA_DB_PORT=$(MIA_DB_PORT) \
		--build-arg A_MIA_DB_NAME=$(MIA_DB_NAME) \
		--build-arg A_MIA_DB_USER=$(MIA_DB_USER) \
		--build-arg A_MIA_DB_PASS=$(MIA_DB_PASS) \
		\
		$(MIA_BUILD_ADDITIONALPARAMETERS) \
		$(PROGRESS) \
		$(if $(TAG_LATEST),-t $(MIA_IMAGE_NAME):latest,) \
		$(if $(TAG_DEV),-t $(MIA_IMAGE_NAME):dev,) \
		$(if $(TAG_PRODUCTION),-t $(MIA_IMAGE_NAME):prod,) \
		$(if $(TAG_VERSION),-t $(MIA_IMAGE_NAME):$(TAG_VERSION),) \
		.
# might be needed for publishing
#	--force-rm
# If TARGET_MULTIPLATFORM empty AND TARGET_PUSH set
	@if [ -z "$(TARGET_MULTIPLATFORM)" ] && [ "$(TARGET_PUSH)" = "true" ]; then \
  		printf "Pushing separately:\n" && \
        docker push $(MIA_IMAGE_NAME) --all-tags; \
    fi

rebuild: # Rebuild container
rebuild: stop remove delete build


run: # Run container (create it if it does not exist)
	@printf "Starting $(_BLUE)$(MIA_CONTAINER_NAME)$(_NORMAL) container.\n"
ifeq ($(OS), Darwin)
ifeq ($(ARCH), arm64)
	@printf " • Running on $(_BLUE)Apple silicon$(_NORMAL).\n"
endif
endif
ifeq ($(MIA_CONTAINER_VOLUME), container)
	@printf " • Using $(_BLUE)container$(_NORMAL) files.\n"
endif
ifeq ($(MIA_CONTAINER_VOLUME), local)
	@printf " • Files $(_BLUE)local$(_NORMAL) files.\n"
endif

	@printf " • Using HTTP port $(_BLUE)$(MIA_CONTAINER_PORT)$(_NORMAL)\n"
ifeq ($(OS), Darwin)
ifeq ($(MDS_APP_NOOPEN),true)
	@printf " • Skipping browser opening as set in $(_BLUE)MIA_DBBACKUP_USESOCKET$(_NORMAL)\n"
else
	@printf " • Opening browser for you at $(_BLUE)$(MIA_CONTAINER_PROTOCOL)://$(MIA_CONTAINER_HOST):$(MIA_CONTAINER_PORT)$(_NORMAL)\n\n"
	@(\
		sleep 2 \
		&& open $(MIA_CONTAINER_PROTOCOL)://$(MIA_CONTAINER_HOST):$(MIA_CONTAINER_PORT) \
	) &
endif
else
	@printf " • Open browser at $(_BLUE)$(MIA_CONTAINER_PROTOCOL)://$(MIA_CONTAINER_HOST):$(MIA_CONTAINER_PORT)$(_NORMAL) to access $(_BLUE)$(MIA_APP_NAME)$(_NORMAL)\n\n"
endif
ifeq ($(MIA_CONTAINER_DAEMONIZE), false)
	@printf "Running in interactive mode. Press $(_BLUE)Ctrl+C$(_NORMAL) to stop.\n\n"
else
	@printf "Running in daemon mode. Run $(_BLUE)make stop$(_NORMAL) to stop.\n\n"
endif
ifeq ($(CONTAINER_RUNNING),true)
	@printf "Container already running. Not restarting\n"
else
ifeq ($(MIA_RUN_OVERRIDE),)
ifeq ($(CONTAINER_EXISTS),true)
	@printf "Starting the container.\n"
	@( \
	docker start \
      $(MIA_CONTAINER_NAME) \
	)
else
	@printf "Creating the container.\n"
	@( \
		docker run \
		  $(DAEMONIZE) \
		  --env-file=.env-default \
		  --env-file=.env \
		  -p $(MIA_CONTAINER_PORT):80 \
		  $(VOLUME) \
		  --name $(MIA_CONTAINER_NAME) \
		  $(MIA_RUN_ENVIRONMENTVARIABLES) \
		  $(MIA_RUN_ADDITIONALPARAMETERS) \
		  $(PLATFORM) \
		  $(MIA_IMAGE_NAME) \
	)
endif
	@printf $(_SUCCESS) "Done"
else
	@printf "Starting using: $(_BLUE)$(MIA_RUN_OVERRIDE)$(_NORMAL)\n"
	@($(MIA_RUN_OVERRIDE))
endif
endif


start: run # Same as \\033[34mrun\\033[0m


stop: # Stop container (if it exists and is running)
ifeq ($(CONTAINER_EXISTS),true)
ifeq ($(CONTAINER_RUNNING),true)
ifeq ($(MIA_STOP_OVERRIDE),)
	@printf "Stopping $(_BLUE)$(MIA_CONTAINER_NAME)$(_NORMAL) container. "
	@docker stop $(MIA_CONTAINER_NAME) &>/dev/null || true && printf $(_SUCCESS) "Done"
else
	@printf "Stopping using: $(_BLUE)$(MIA_STOP_OVERRIDE)$(_NORMAL)\n"
	@($(MIA_STOP_OVERRIDE))
endif
stop: CONTAINER_RUNNING=false
else
	@printf "Container $(MIA_CONTAINER_NAME) is not running\n"
endif
else
	@printf "Container $(MIA_CONTAINER_NAME) does not exist\n"
endif


restart: stop start # \\033[34mstop\\033[0m + \\033[34mstart\\033[0m


remove: # Remove container (if it exists)
ifeq ($(CONTAINER_EXISTS),true)
ifeq ($(CONTAINER_RUNNING),true)
remove: stop
endif
	@printf "Removing $(_BLUE)$(MIA_CONTAINER_NAME)$(_NORMAL) container. "
	@docker rm $(MIA_CONTAINER_NAME) &>/dev/null || true && printf $(_SUCCESS) "Done"
else
	@printf "Container $(_RED)$(MIA_CONTAINER_NAME)$(_NORMAL) does not exist\n"
endif


delete: # Delete container image
ifeq ($(CONTAINER_RUNNING),false)
ifeq ($(CONTAINER_EXISTS),false)
	@(printf "Deleting $(_BLUE)$(MIA_CONTAINER_NAME)$(_NORMAL) image")
	@(docker image rm $(MIA_IMAGE_NAME) &>/dev/null || true && printf " - Done\n")
else
	@printf "Container $(_RED)$(MIA_CONTAINER_NAME)$(_NORMAL) exists; cannot delete image\n"
	@printf "Run $(_BLUE)make remove$(_NORMAL) to remove\n"
endif
else
	@printf "Container $(_RED)$(MIA_CONTAINER_NAME)$(_NORMAL) is running; cannot delete image\n"
	@printf "Run $(_BLUE)make stop$(_NORMAL) to stop\n"
endif


clean: stop remove delete


release: # Release container image to registry
ifndef MIA_IMAGE_TAG
	@printf "Make sure you set $(_BLUE)MIA_IMAGE_TAG$(_NORMAL) in .env-default\n"
else
release: settag
release: confirmrebuild
release: confirmreplace
release: applygittag
ifeq ($(OS), Darwin)
ifeq ($(ARCH), arm64)
release: TARGET_MULTIPLATFORM=true
endif
endif
release: TARGET_PUSH=true
release: build
endif


settag:
ifneq ($(filter $(BRANCH),master main),$(BRANCH))
	@printf "$(tput bold)You are attempting to release git branch $(_BLUE)$(BRANCH)$(_NORMAL)$(tput sgr0)\n"
	@printf "This will be published to branch $(_BLUE)dev$(_NORMAL) instead of 'prod'\n"
	@printf "Your version will be appended with the timestamp: $(_BLUE)$(TAG_VERSION)$(_NORMAL)\n"
release: TAG_PRODUCTION=
release: TAG_VERSION=$(MIA_IMAGE_TAG)-$(shell date '+%Y%m%d.%H%M.%S')
release: TAG_GIT:=b$(TAG_VERSION)
else
release: TAG_PRODUCTION=true
release: TAG_LATEST=true
release: TAG_DEV=
release: TAG_VERSION=$(MIA_IMAGE_TAG)
release: TAG_GIT:=$(TAG_VERSION)
endif


confirmrebuild:
	@read -s -p "This will rebuild and release to ${MIA_IMAGE_NAME}:${TAG_VERSION}. Do you confirm [y/N]? " -n 1 -r ans && ans=$${ans:-N} ; \
    if [ $${ans} = y ] || [ $${ans} = Y ]; then \
        printf $(_SUCCESS) "YES" ; \
    else \
        printf $(_DANGER) "NO" ; \
        exit 1; \
    fi


confirmreplace:
	@if [ $$(docker manifest inspect ${MIA_IMAGE_NAME}:${TAG_VERSION} >/dev/null 2>&1; echo $$?) != "1" ]; then \
		read -s -p "Another release of ${MIA_IMAGE_NAME}:${TAG_VERSION} already exists. Do you want to replace it [y/N]? " -n 1 -r ans && ans=$${ans:-N} ; \
		if [ $${ans} = y ] || [ $${ans} = Y ]; then \
			printf $(_SUCCESS) "YES" ; \
		else \
			printf $(_DANGER) "NO" ; \
			exit 1; \
		fi \
	fi


applygittag:
ifneq ($(filter $(BRANCH),master main),$(BRANCH))
ifeq ($(MIA_REPOSITORY_APPLYTAG), true)
	@printf "\nApply tag $(_BLUE)${TAG_GIT}$(_NORMAL) to git repository\n"
	git tag $(TAG_GIT)
	git push `git remote` $(TAG_GIT) --force
endif
endif


publish: release	# Same as \\033[34mrelease\\033[\\033[34m


deploy: # Deploy to server \\033[34mserver=<server>\\033[0m, or \\033[34mMIA_DEPLOY_DEFAULT_SERVER\\033[0m by default
deploy: confirmdeploy
ifndef server
ifdef MIA_DEPLOY_DEFAULT_SERVER
	@printf "Using default server\n"
	$(call deploy_to_server,$(MIA_DEPLOY_DEFAULT_SERVER))
else
	@printf "Usage: make deploy server=<server>\n"
	@printf "Available servers: $(MIA_DEPLOY_SERVERS)\n"
endif
else
ifeq ($(filter $(server),$(MIA_DEPLOY_SERVERS)),)
	@printf "Server $(_BLUE)$(server)$(_NORMAL) not found in configuration\n"
	@printf "Available servers: $(_BLUE)$(MIA_DEPLOY_SERVERS)$(_NORMAL)\n"
else
	$(call deploy_to_server,$(server))
endif
endif


confirmdeploy:
	@read -s -p "This will deploy ${MIA_CONTAINER_NAME} to ${server}. Do you confirm [y/N]? " -n 1 -r ans && ans=$${ans:-N} ; \
    if [ $${ans} = y ] || [ $${ans} = Y ]; then \
        printf $(_SUCCESS) "YES" ; \
    else \
        printf $(_DANGER) "NO" ; \
        exit 1; \
    fi
ifeq ($(server), production)
	@read -s -p "I said: this will deploy to PRODUCTION. Are you really sure [y/n]? " -n 1 -r ans && ans=$${ans:-N} ; \
	if [ $${ans} = y ] || [ $${ans} = Y ]; then \
		printf $(_SUCCESS) "YES" ; \
	else \
		printf $(_DANGER) "NO" ; \
		exit 1; \
	fi
endif


define deploy_to_server
	@printf "Deploying to $(_BLUE)$1$(_NORMAL)\n"
	@(ssh -i $(MIA_DEPLOY_$1_KEYFILE) $(MIA_DEPLOY_$1_HOST) "$(MIA_DEPLOY_$1_COMMAND)")
endef


login: # Login to server \\033[34mserver=<server>\\033[0m, or \\033[34mMIA_DEPLOY_DEFAULT_SERVER\\033[0m by default
ifndef server
ifdef MIA_DEPLOY_DEFAULT_SERVER
	@printf "Using default server\n"
	$(call login_to_server,$(MIA_DEPLOY_DEFAULT_SERVER))
else
	@printf "Usage: make login server=<server>\n"
	@printf "Available servers: $(MIA_DEPLOY_SERVERS)\n"
endif
else
ifeq ($(filter $(server),$(MIA_DEPLOY_SERVERS)),)
	@printf "Server $(_BLUE)$(server)$(_NORMAL) not found in configuration\n"
	@printf "Available servers: $(_BLUE)$(MIA_DEPLOY_SERVERS)$(_NORMAL)\n"
else
	$(call login_to_server,$(server))
endif
endif


define login_to_server
	@printf "Logging into $(_BLUE)$1$(_NORMAL)\n"
	@(ssh -i $(MIA_DEPLOY_$1_KEYFILE) $(MIA_DEPLOY_$1_HOST))
endef


# #9 dbbackup
file ?= $(MIA_DB_USER).sql.gz
MIA_DBBACKUP_HOST ?= $(MIA_DB_HOST)
MIA_DBBACKUP_PORT ?= $(MIA_DB_PORT)
MIA_DBBACKUP_NAME ?= $(MIA_DB_NAME)
MIA_DBBACKUP_USER ?= $(MIA_DB_USER)
MIA_DBBACKUP_PASS ?= $(MIA_DB_PASS)

backup: # Backup database \\033[34mfile=<path/to/file.sql.gz>\\033[0m, or \\033[34mMIA_DB_NAME.sql.gz\\033[0m by default
ifndef file
	@printf "Usage: make backup file=<path/to/file.sql.gz>\n"
	@printf "Database that will be backed up: $(_BLUE)$(MIA_DB_NAME)$(_NORMAL)\n"
else
	@printf "Backing up database $(_BLUE)$(MIA_DB_NAME)$(_NORMAL) to file $(_BLUE)$(file)$(_NORMAL)\n"
ifeq ($(MIA_DBBACKUP_USESOCKET), true)
	@mysqldump --user=$(MIA_DBBACKUP_USER) --password=$(MIA_DBBACKUP_PASS) $(MIA_DBBACKUP_NAME) | gzip > $(file)
else
	@mysqldump --host=$(MIA_DBBACKUP_HOST) --port=$(MIA_DBBACKUP_PORT) --user=$(MIA_DBBACKUP_USER) --password=$(MIA_DBBACKUP_PASS) $(MIA_DBBACKUP_NAME) | gzip > $(file)
endif
	@printf "Done\n"
endif

restore: # Restore database \\033[34mfile=<path/to/file.sql.gz>\\033[0m, or \\033[34mMIA_DB_NAME.sql.gz\\033[0m by default
ifndef file
	@printf "Usage: make restore file=<path/to/file.sql.gz>\n"
	@printf "Database that will be restored up: $(_BLUE)$(MIA_DB_NAME)$(_NORMAL)\n"
else
	@printf "Restoring file $(_BLUE)$(file)$(_NORMAL) to database $(_BLUE)$(MIA_DB_NAME)$(_NORMAL)\n"
ifeq ($(MIA_DBBACKUP_USESOCKET), true)
	@gunzip < $(file) | mysql --user=$(MIA_DBBACKUP_USER) --password=$(MIA_DBBACKUP_PASS) $(MIA_DBBACKUP_NAME)
else
	@gunzip < $(file) | mysql --host=$(MIA_DBBACKUP_HOST) --port=$(MIA_DBBACKUP_PORT) --user=$(MIA_DBBACKUP_USER) --password=$(MIA_DBBACKUP_PASS) $(MIA_DBBACKUP_NAME)
endif
	@printf "Done\n"
endif

check:
ifdef MIA_CICD_CHECK
	$(MIA_CICD_CHECK)
else
	@echo "No CI/CD Check command provided"
endif

fix:
ifdef MIA_CICD_FIX
	$(MIA_CICD_FIX)
else
	@echo "No CI/CD Fix command provided"
endif

mds_update: # Update MDS script (git submodule)
	@printf "Updating MDS\n"
	git submodule update --init --force --remote


help: # Show this help
	@printf "$(_BLUE)MiA Deployment Script$(_NORMAL) (MDS) v$(MDS_VERSION)\n"
	@grep -E '^[a-zA-Z0-9 -]+:.*#'  makefile | sort | while read -r l; do printf "\033[1;32m$$(echo -e $$l | cut -f 1 -d':')\t\033[00m:$$(echo -e $$l | cut -f 2- -d'#')\n"; done
